const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const {
    nanoid
} = require('nanoid');
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB({
    apiVersion: '2012-08-10'
});
const documentClient = new AWS.DynamoDB.DocumentClient();
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
        extended: true
    }));

app.use(cors());
app.options('*', cors());

app.get('/', (req, res) => res.send({
        'value1': 'uclub server'
    })); // run the server locally

/*
insert a new post in the specified category
INPUT:
-> id category;
-> post type;
-> subject of the post;
-> reward for the best(most upvoted) reply;
-> tags;
-> title;
-> content of the post;
-> author;
OUTPUT:
->  none
 */

app.post('/post', function (req, res) {
    var post_type = "post";
    var anon = req.body.is_anonymous;
    var reward = req.body.reward;
    var tags = req.body.tags;
    var question = req.body.question;
    var author = req.body.idA;
    var best_answer = '';
    var votes = [];
    var date_creation = (new Date()).toISOString();
    var replies = [];
    var award_method = req.body.award_method;

    params = {
        TableName: 'Post',
        Item: {
            'id': nanoid(5),
            'type': post_type,
            'is_anonymous': anon,
            'reward': reward,
            'category': req.body.idC,
            'tags': tags,
            'best_answer': best_answer,
            'question': question,
            'author': author,
            'votes': votes,
            'date_creation': date_creation,
            'replies': replies,
            'award_method': award_method,
            "status": "ok"
        }

    };
    documentClient.put(params, function (err, data) {
        if (err) {
            res.send(err)
        } else {
            res.send('successo');
        }
    });
})

app.get('/user/:idUser/nexus/', function (req, res) {
    //prendo le categorie
    var params = {
        TableName: 'Utenti',
        KeyConditionExpression: "id = :id",
        ProjectionExpression: "categories",
        ExpressionAttributeValues: {
            ":id": req.params.idUser
        }
    };

    documentClient.query(params, function (err, data) {
        if (err) {
            res.send(err);
        } else {
            //sistemo le categorie in modo da usarle nel db
            var catValues = data['Items'][0]['categories'];
            var catObject = {};
            var index = 0;
            catValues.forEach(function (value) {
                index++;
                var catKey = ":titlevalue" + index;
                catObject[catKey.toString()] = value;
            });

            params = {
                TableName: "Post",
                IndexName: "category_index",
                KeyConditions: {
                    status: {
                        ComparisonOperator: "EQ",
                        AttributeValueList: [
                            "ok"
                        ]
                    }
                },
                FilterExpression: "category IN (" + Object.keys(catObject).toString() + ")",
                ExpressionAttributeValues: catObject,
                ScanIndexForward: false,
                Limit: 50
            };
            //prendo i post dalle categorie
            documentClient.query(params, async function (err, data) {
                if (err) {
                    res.send(err);
                } else {
					//cerco di trovare profile_pic
                    for (var i = 0; i < data['Items'].length; i++) {
                        params = {
                            TableName: 'Utenti',
                            ProjectionExpression: "nome,cognome,profile_pic",
                            KeyConditionExpression: "id = :id",
                            ExpressionAttributeValues: {
                                ":id": data['Items'][0]['author']
                            }
                        };
						var profile = await documentClient.query(params).promise();
						data['Items'][i]['profile_pic'] = profile['Items'][0]['profile_pic'];
						data['Items'][i]['nome'] = profile['Items'][0]['nome'];
						data['Items'][i]['cognome'] = profile['Items'][0]['cognome'];
					}
					res.send(data['Items']); 
                }
            })
        }
    });
});

module.exports = app;
